# Basic git setup

## SSH
To access your repositories you should use [SSH keys](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair).
If you want to create a new one make sure you got `openssh` installed on your system and run the following command in a terminal:

    ssh-keygen -t ed25519 -C "" -N "" -f ~/.ssh/id_ed25519
    Alternative: ssh-keygen -t rsa -b 4096 -C "" -N "" -f ~/.ssh/id_rsa

Then copy your public key (**important: never upload your private key somewhere!**) via:

    cat ~/.ssh/id_ed25519.pub

Insert it into the input field at [GitLab](https://gitlab.com/profile/keys) and click on "Add key" or on [GitHub](https://github.com/settings/keys) using "New SSH key".

To make sure that the correct SSH key is used when accessing the GitLab or Github instance add the following to your config at `~/.ssh/config`:

    Host gitlab.com
        HostName gitlab.com
        Port 22
        User <your username in GitLab>
        Preferredauthentications publickey
        IdentityFile ~/.ssh/id_ed25519
        AddKeysToAgent yes

    Host github.com
        HostName github.com
        Port 22
        User <your username in Github>
        Preferredauthentications publickey
        IdentitiyFile ~/.ssh/id_ed25519
    
## GPG
If you want your commits to be signed you need to generate a new [GPG key](https://gitlab.com/help/user/project/repository/gpg_signed_commits/index.md) for your email if you don't have one yet.
In order to do that you need to have `gpg` installed and then run:

    gpg --gen-key
    
During the creation use RSA as the type of encryption and 4096 as the key length. When finished run the following command to retrieve the public key ID.

    gpg --list-secret-keys --keyid-format LONG <your email> | grep 'sec' | cut -d'/' -f 2 | cut -d' ' -f 1 | head -n 1 
    
Export the public key via:

    gpg --armor --export <key id>

Then enter the copied text at [GitLab](https://gitlab.com/profile/gpg_keys) and press "Add key" or [Github](https://github.com/settings/keys) with "New GPG key".

## Git Config
Finally you should set up your git settings. 
These can either be global or for each repository individually. 
Depending on that you need the `--global`-flag or not. 
Below is a list of recommended options:

    git config (--global) user.email "<youremail>"
    git config (--global) user.name "<FirstName LastName>"
    git config (--global) user.signingkey <key id>
    git config (--global) commit.gpgsign true
    git config (--global) pull.rebase true
    git config (--global) color.ui true
    git config (--global) diff.renamelimit 99999
    git config (--global) http.postBuffer 524288000
    git config (--global) remote.origin.prune true
    git config (--global) inspector.format htmlembedded
    git config (--global) inspector.list-file-types true
    git config (--global) inspector.metrics true
    git config (--global) inspector.responsibilities true
    git config (--global) inspector.timeline true
    git config (--global) inspector.weeks true
    git config (--global) inspector.file-types **
    git config (--global) inspector.hard true
    git config (--global) inspector.grading true

You can also edit the local config manually at `.git/config` in your repository or the global one at `~/.gitconfig`.

## Git Hooks  
Git lets you run scripts before/after certain actions like running `./gradlew test` before actually creating a commit. 
You can find more about them [here](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks).  
Additionally some examples are in the hooks-folder in this repository.
